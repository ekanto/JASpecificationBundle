<?php

namespace eKanto\JsonApi\v1_0\SpecificationBundle\AOP\Pointcut;

use JMS\AopBundle\Aop\PointcutInterface;

class ExceptionAwarePointcut implements PointcutInterface
{
	public function matchesClass(\ReflectionClass $class)
	{
		return $class->isSubclassOf('eKanto\JsonApi\v1_0\SpecificationBundle\Controller\AbstractExceptionAwareController');

	}

	public function matchesMethod(\ReflectionMethod $method)
	{
		return true;
	}
}