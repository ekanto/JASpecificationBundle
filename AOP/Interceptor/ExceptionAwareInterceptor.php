<?php

namespace eKanto\JsonApi\v1_0\SpecificationBundle\AOP\Interceptor;

use CG\Proxy\MethodInterceptorInterface;
use CG\Proxy\MethodInvocation;
use eKanto\JsonApi\v1_0\SpecificationBundle\Exception\Exception;
use eKanto\JsonApi\v1_0\SpecificationBundle\Document\Factory\ErrorFactory;

class ExceptionAwareInterceptor implements MethodInterceptorInterface
{
	public function intercept(MethodInvocation $invocation)
	{
		try {
			return $invocation->proceed();
		} catch (\Symfony\Component\HttpKernel\Exception\HttpException $e){
			return ErrorFactory::madeResponse($e);
		}
	}
}