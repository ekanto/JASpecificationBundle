<?php

namespace eKanto\JsonApi\v1_0\SpecificationBundle\Document\Object;

class Error implements \JsonSerializable
{
	
	/**
	 * 
	 * @var string
	 */
	private $id;
	
	private $links;
	
	/**
	 * 
	 * @var string
	 */
	private $status;
	
	/**
	 * 
	 * @var string
	 */	
	private $code;
	
	/**
	 * 
	 * @var string
	 */	
	private $title;
	
	/**
	 * 
	 * @var string
	 */	
	private $detail;
	
	/**
	 * 
	 * @var string
	 */
	private $source;
	
	private $meta;

	public function __construct(string $id = null)
	{
        $this->id = $id;
		if ( null == $id ) {
			$this->id = uniqid();
		}
	}

	public function jsonSerialize()
	{
		return array(
			'id' => $this->id,
			'status' => $this->getStatus(),
			'code' => $this->getCode(),
			'title' => $this->getTitle(),
			'detail' => $this->getDetail()
		);
	}

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * @param string $detail
     *
     * @return self
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return self
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }
}