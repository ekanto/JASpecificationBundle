<?php

namespace eKanto\JsonApi\v1_0\SpecificationBundle\Document;

use eKanto\JsonApi\v1_0\SpecificationBundle\Document\Object\{Meta, Error};

class TopLevel implements \JsonSerializable
{
	/**
	 * the document’s “primary data”
	 * @var array
	 */
	private $data;

	/**
	 * an array of error objects
	 * @var array
	 */
	private $errors =  array();

	/**
	 * a meta object that contains non-standard meta-information.
	 * @var Meta
	 */
	private $meta;

	public function jsonSerialize()
	{

		$topLevel = array('meta' => $this->getMeta());
		$errors = $this->getErrors();

		// The members data and errors MUST NOT coexist in the same document.
		if ( !empty( $errors ) ) {
			$topLevel['errors'] = $errors;

			return $topLevel;
		}

		$topLevel['data'] = $this->getData();

		return $topLevel;
	}

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     *
     * @return self
     */
    public function setErrors(array $errors)
    {
        $this->errors = $errors;

        return $this;
    }

    public function pushError(Error $error)
    {
        array_push($this->errors, $error);
    }

    /**
     * @return Meta
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param Meta $meta
     *
     * @return self
     */
    public function setMeta(Meta $meta)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     *
     * @return self
     */
    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }
}