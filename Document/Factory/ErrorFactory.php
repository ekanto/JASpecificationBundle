<?php

namespace eKanto\JsonApi\v1_0\SpecificationBundle\Document\Factory;

use eKanto\JsonApi\v1_0\SpecificationBundle\HttpException\HttpException;
use eKanto\JsonApi\v1_0\SpecificationBundle\Response\JsonResponse;
use eKanto\JsonApi\v1_0\SpecificationBundle\Document\{TopLevel, Object\Error};

class ErrorFactory
{
	public static function madeResponse(HttpException $httpException): JsonResponse 
	{
		$currentException = $httpException;
		$httpExceptions = array();

		do{
			array_push($httpExceptions, $currentException);
			$currentException = $currentException->getPrevious();
		}
		while(null != $currentException);
		
		$topLevel = new TopLevel();
		$firstErrorStatus = null;
		foreach ($httpExceptions as $httpException) {
			$error = new Error();
			$firstErrorStatus = $httpException->getStatusCode();
			$error->setCode( $httpException->getCode() );
			$error->setStatus( $httpException->getStatusCode() );
			$error->setTitle( $httpException->getTitle() );
			$error->setDetail( $httpException->getDetail() );
			$topLevel->pushError($error);
		}

		return new JsonResponse($topLevel, $firstErrorStatus);
	}
}