<?php

namespace eKanto\JsonApi\v1_0\SpecificationBundle\HttpException;

trait HttpExceptionTrait
{

	protected $exceptionStack = array();

	public function createHttpException(int $statusCode, string $title, string $detail, $internalCode = 0, $throw = true)
	{

		$lastException = end($this->exceptionStack);

		if ( FALSE == $lastException) {
			$lastException = null;
		}

		$exception = new HttpException( $statusCode, $title, $detail, $lastException, $internalCode );

		if ( null != $lastException ) {
			$lastCategory = substr($lastException->getStatusCode(), 0, 1);
			$currentCategory = substr($exception->getStatusCode(), 0, 1);
			if ( $lastCategory != $currentCategory ) {
				throw new \Exception("Can not stack exceptions of different category");
			}
		}

		if ( $throw ) {
			throw $exception;
		}

		array_push($this->exceptionStack, $exception);

		return $exception;
	}

	public function throwStackedExceptions()
	{
		
		if ( !empty( $this->exceptionStack ) ) {
			$exception = end( $this->exceptionStack );
			throw $exception;
		}

	}

	public function purgeException()
	{
		$this->exceptionStack = array();
	}

	/**
	 * Notice: createNotFoundException allredy exist in supperclass
	 * @param  string  $title        a short, human-readable summary of the problem that SHOULD NOT change from
	 * @param  string  $detail       a human-readable explanation specific to this occurrence of the problem. Like title, this field’s value can be localized.
	 * @param  integer $internalCode an application-specific error code.
	 * @param  boolean $throw        set to false if you need to stack exception and throw it later
	 * @return HttpException         
	 */
	public function generateNotFoundException(string $title, string $detail, $internalCode = 0, $throw = true)
	{
		return $this->createHttpException(404, $title, $detail, $internalCode, $throw);
	}

	public function generateBadRequestException(string $title, string $detail, $internalCode = 0, $throw = true)
	{
		return $this->createHttpException(400, $title, $detail, $internalCode, $throw);		
	}

	public function generateAccessDeniedException(string $title, string $detail, $internalCode = 0, $throw = true)
	{
		return $this->createHttpException(403, $title, $detail, $internalCode, $throw);		
	}
}