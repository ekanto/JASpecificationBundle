<?php

namespace eKanto\JsonApi\v1_0\SpecificationBundle\HttpException;

use Symfony\Component\HttpKernel\Exception\HttpException as BaseHttpException;

class HttpException extends BaseHttpException 
{
	/**
	 * A short, human-readable summary of the problem that SHOULD NOT change from occurrence to occurrence of the problem, except for purposes of localization.
	 * @var string
	 */
	private $title;

	/**
	 * A human-readable explanation specific to this occurrence of the problem. Like title, this field’s value can be localized.
	 * @var string
	 */
	private $detail;

	public function __construct($statusCode, $title = null, $detail = null, \Exception $previous = null, $code = 0)
	{
		$this->title = $title;
		$this->detail = $detail;
		parent::__construct($statusCode, $title, $previous, array(), $code);
	}

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }
}