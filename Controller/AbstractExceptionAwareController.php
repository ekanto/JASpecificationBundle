<?php

namespace eKanto\JsonApi\v1_0\SpecificationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class AbstractExceptionAwareController extends Controller
{
	use \eKanto\JsonApi\v1_0\SpecificationBundle\HttpException\HttpExceptionTrait;
}